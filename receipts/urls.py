from django.urls import path

from receipts.views import (
    ReceiptListView,
    ReceiptCreateView,
    ExpenseCategoryCreateView,
    AccountsCreateView,
    AccountsListView,
    ExpenseCategoryListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path(
        "accounts/create/",
        AccountsCreateView.as_view(),
        name="accounts_create",
    ),
    path("accounts/", AccountsListView.as_view(), name="accounts_list"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="expense_category_create",
    ),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="expense_category_list",
    ),
]
