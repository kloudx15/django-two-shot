from msilib.schema import ListView
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import ExpenseCategory, Account, Receipt

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = [
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    ]

    def form_valid(self, form):
        receipts = form.save(commit=False)
        receipts.purchaser = self.request.user
        receipts.save()
        return redirect("home")

    def get_success_url(self):
        return reverse_lazy("home")


class ExpenseCategoryListView(ListView):
    model = ExpenseCategory
    template_name = "receipts/categories/list.html"

    def get_query_set(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(CreateView):
    model = ExpenseCategory
    template_name = "receipts/categories/create.html"
    fields = [
        "name",
        "owner",
    ]

    def form_valid(self, form):
        categories = form.save(commit=False)
        categories.owner = self.request.user
        categories.save()
        return redirect("expense_category_list")


class AccountsListView(ListView):
    model = Account
    template_name = "receipts/accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountsCreateView(CreateView):
    model = Account
    template_name = "receipts/accounts/create.html"
    fields = ["name", "number", "owner"]

    def form_valid(self, form):
        accounts = form.save(commit=False)
        accounts.owner = self.request.user
        accounts.save()
        return redirect("accounts_list")


# def get_success_url(self):
#         return reverse_lazy("expense_category_list")
