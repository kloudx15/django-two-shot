# import models, views, and functions here
from django.urls import path
from accounts.views import (
    LoginLoginView,
    LogoutLogoutView,
    signup,
)

# url path patterns here

urlpatterns = [
    path("login/", LoginLoginView.as_view(), name="login"),
    path("logout/", LogoutLogoutView.as_view(), name="logout"),
    path("signup/", signup, name="signup"),
]
